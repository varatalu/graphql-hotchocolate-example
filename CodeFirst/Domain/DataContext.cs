using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using CodeFirst.GraphQLSpecific;

namespace CodeFirst.Domain
{
    /// <summary>
    /// DbContext / Repository Mockup
    /// </summary>
    public class DataContext
    {
        private readonly List<PersonDbRecord> people = new List<PersonDbRecord>();
        private readonly List<CarDbRecord> cars = new List<CarDbRecord>();
        public DataContext()
        {
            #region Data

            people.Add(new PersonDbRecord
            {
                Id = 1,
                Name = "Luke",
                DateOfBirth = DateTime.Parse("1/12/1900"),
                Gender = Gender.MALE,
                HairColor = HairColor.Blonde,
                Height = 1.80m, //m lõpus tähendab decimal / sarnaselt 1.80f oleks float, 1.80d oleks double
                Weight = 95.6m,
                FriendIdsList = new List<int>{3,5}
            });

            people.Add(new PersonDbRecord
            {
                Id = 2,
                Name = "Mary",
                DateOfBirth = DateTime.Parse("1/12/1850"),
                Gender = Gender.FEMALE,
                HairColor = HairColor.Brunette,
                Height = 1.65m,
                Weight = 60.6m,
                FriendIdsList = new List<int>{3,4}
            });


            people.Add(new PersonDbRecord
            {
                Id = 3,
                Name = "Thomas",
                DateOfBirth = DateTime.Parse("1/12/1920"),
                Gender = Gender.MALE,
                HairColor = HairColor.Ginger,
                Height = 1.80m,
                Weight = 95.6m,
                FriendIdsList = new List<int>{1,2,4}
            });

            people.Add(new PersonDbRecord
            {
                Id = 4,
                Name = "Sarah",
                DateOfBirth = DateTime.Parse("1/12/1930"),
                Gender = Gender.FEMALE,
                HairColor = HairColor.Other,
                Height = 2.10m,
                Weight = 92.0m,
                FriendIdsList = new List<int>{2,3}
            });

            people.Add(new PersonDbRecord
            {
                Id = 5,
                Name = "Jeff",
                DateOfBirth = DateTime.Parse("1/12/1940"),
                Gender = Gender.MALE,
                HairColor = HairColor.Brunette,
                Height = 1.66m,
                Weight = 120.0m,
                FriendIdsList = new List<int>{1}
            });


            cars.Add(new CarDbRecord
            {
                Id = 1,
                ModelYear = 2005,
                Manufacturer = "Audi",
                ModelName = "A6",
                Color = KnownColor.Black,
                OwnerId = 1
            });

            cars.Add(new CarDbRecord
            {
                Id = 2,
                ModelYear = 2012,
                Manufacturer = "Mercedes-Benz",
                ModelName = "S500",
                Color = KnownColor.Coral,
                OwnerId = 1
            });

            cars.Add(new CarDbRecord
            {
                Id = 3,
                ModelYear = 2014,
                Manufacturer = "Mitsubishi",
                ModelName = "Lancer",
                Color = KnownColor.White,
                OwnerId = 2
            });

            cars.Add(new CarDbRecord
            {
                Id = 4,
                ModelYear = 2000,
                Manufacturer = "Honda",
                ModelName = "Civic",
                Color = KnownColor.DarkGreen,
                OwnerId = 2
            });

            cars.Add(new CarDbRecord
            {
                Id = 5,
                ModelYear = 2005,
                Manufacturer = "Subaru",
                ModelName = "Impreza",
                Color = KnownColor.Black,
                OwnerId = 2
            });

            cars.Add(new CarDbRecord
            {
                Id = 6,
                ModelYear = 2010,
                Manufacturer = "Maserati",
                ModelName = "GranTurismo",
                Color = KnownColor.Red,
                OwnerId = 2
            });

            cars.Add(new CarDbRecord
            {
                Id = 7,
                ModelYear = 1943,
                Manufacturer = "Volkswagen",
                ModelName = "Kübelwagen",
                Color = KnownColor.Beige,
                OwnerId = 3
            });

            cars.Add(new CarDbRecord
            {
                Id = 8,
                ModelYear = 1957,
                Manufacturer = "Mercedes-Benz",
                ModelName = "300 SL",
                Color = KnownColor.DarkGreen,
                OwnerId = 3
            });

            cars.Add(new CarDbRecord
            {
                Id = 8,
                ModelYear = 1957,
                Manufacturer = "Mercedes-Benz",
                ModelName = "300 SL",
                Color = KnownColor.DarkGreen,
                OwnerId = 3
            });

            cars.Add(new CarDbRecord
            {
                Id = 9,
                ModelYear = 1976,
                Manufacturer = "Porsche",
                ModelName = "911 Turbo",
                Color = KnownColor.DarkGray,
                OwnerId = 3
            });

            cars.Add(new CarDbRecord
            {
                Id = 10,
                ModelYear = 1999,
                Manufacturer = "Opel",
                ModelName = "Astra",
                Color = KnownColor.Blue,
                OwnerId = 4
            });

            cars.Add(new CarDbRecord
            {
                Id = 11,
                ModelYear = 1998,
                Manufacturer = "Opel",
                ModelName = "Astra",
                Color = KnownColor.Gold,
                OwnerId = 4
            });

            cars.Add(new CarDbRecord
            {
                Id = 12,
                ModelYear = 1999,
                Manufacturer = "Opel",
                ModelName = "Astra",
                Color = KnownColor.White,
                OwnerId = 4
            });

            cars.Add(new CarDbRecord
            {
                Id = 13,
                ModelYear = 1999,
                Manufacturer = "Opel",
                ModelName = "Astra",
                Color = KnownColor.Orange,
                OwnerId = 4
            });

            cars.Add(new CarDbRecord
            {
                Id = 14,
                ModelYear = 1986,
                Manufacturer = "Ford",
                ModelName = "Sierra",
                Color = KnownColor.Blue,
                OwnerId = 5
            });

            cars.Add(new CarDbRecord
            {
                Id = 15,
                ModelYear = 1986,
                Manufacturer = "Ford",
                ModelName = "Sierra",
                Color = KnownColor.Cyan,
                OwnerId = 5
            });

            cars.Add(new CarDbRecord
            {
                Id = 16,
                ModelYear = 1986,
                Manufacturer = "Ford",
                ModelName = "Sierra",
                Color = KnownColor.Blue,
                OwnerId = 5
            });

            cars.Add(new CarDbRecord
            {
                Id = 17,
                ModelYear = 1986,
                Manufacturer = "Ford",
                ModelName = "Sierra",
                Color = KnownColor.Purple,
                OwnerId = 5
            });

            cars.Add(new CarDbRecord
            {
                Id = 18,
                ModelYear = 1986,
                Manufacturer = "Ford",
                ModelName = "Sierra",
                Color = KnownColor.Yellow,
                OwnerId = 5
            });

            cars.Add(new CarDbRecord
            {
                Id = 19,
                ModelYear = 1986,
                Manufacturer = "Ford",
                ModelName = "Sierra",
                Color = KnownColor.Green,
                OwnerId = 5
            });

            cars.Add(new CarDbRecord
            {
                Id = 20,
                ModelYear = 1986,
                Manufacturer = "Ford",
                ModelName = "Sierra",
                Color = KnownColor.Silver,
                OwnerId = 5
            });

            cars.Add(new CarDbRecord
            {
                Id = 21,
                ModelYear = 1986,
                Manufacturer = "Ford",
                ModelName = "Sierra",
                Color = KnownColor.Orange,
                OwnerId = 5
            });


            #endregion
        }

        public IEnumerable<PersonDbRecord> GetPeople(int? id, string? name, Gender? gender, HairColor? hairColor)
        {
            var query = people.AsQueryable();
           
            if (id != null)
                query = query.Where(x => x.Id == id);

            if (name != null)
                query = query.Where(x => x.Name == name);

            if (gender != null)
                query = query.Where(x => x.Gender == gender);

            if (hairColor != null)
                query = query.Where(x => x.HairColor == hairColor);

            return query;
        }

        public PersonDbRecord GetPerson(int id)
        {
            var query = people.AsQueryable();

            var person = people.Find(x=>x.Id == id);

            return person;
        }

        public IEnumerable<CarDbRecord> GetCars(int? id, int? modelYear, KnownColor? color, string? modelName, string? manufacturer)
        {
            var query = cars.AsQueryable();

            if (id != null)
                query = query.Where(x => x.Id == id);

            if (modelYear != null)
                query = query.Where(x => x.ModelYear == modelYear);

            if (color != null)
                query = query.Where(x => x.Color == color);

            if (modelName != null)
                query = query.Where(x => x.ModelName == modelName);

            if (manufacturer != null)
                query = query.Where(x => x.Manufacturer == manufacturer);

            return query;
        }

        public PersonDbRecord AddPerson(PersonDbRecord person)
        {
            person.Id = people.Count + 1;
            people.Add(person);

            foreach (var car in person.Cars)
            {
                car.OwnerId = person.Id;
            }
            AddCars(person.Cars);
         

            foreach (var friend in person.Friends)
            {
                if (friend.Id == 0) //loob uue
                {
                    friend.Id = people.Count + 1;
                    AddPerson(friend);
                    person.FriendIdsList.Add(friend.Id);
                }
                else
                {
                    person.FriendIdsList.Add(friend.Id);
                }
            }

            return person;
        }

        public CarDbRecord AddCar(CarDbRecord car)
        {
            car.Id = cars.Count + 1;
            cars.Add(car);
            return car;
        }

        public void AddCars(ICollection<CarDbRecord> carsCollection)
        {
            cars.AddRange(carsCollection);
        }

        public CarDbRecord ChangeCarOwner(int carId,int newOwnerId)
        {
            var car = cars.Find(x => x.Id == carId);
            var newOwner = people.Find(x => x.Id == newOwnerId);

            if (newOwner == null || car == null)
                throw new Exception("car or person doesn't exist");


            car.OwnerId = newOwner.Id;
            return car;
        }

        public PersonDbRecord AddFriend(int personId,int friendId)
        {
            var p = people.Find(x => x.Id == personId);
            var f = people.Find(x => x.Id == friendId);


            if (p.FriendIdsList.Contains(friendId) == false)
            {
                p.FriendIdsList.Add(friendId);
                f.FriendIdsList.Add(personId);
            }
               
               
            return p;
        }
    }
}
