namespace CodeFirst.GraphQLSpecific
{
    public enum Gender
    {
        MALE = 1,
        FEMALE = 2
    }

    public enum HairColor
    {
        Brunette = 1,
        Blonde = 2,
        Ginger = 3,
        Other = 4
    }
}