﻿example queries

mutation{
  createCar(car:{
    modelName:"aa"
    manufacturer:"bb"
    modelYear:123
    ownerId:2
    id:15
    color:BLUE
  })
}


query {
  people(id:2) {
    id
    friends{
      id
      name
    }
  }
}
