﻿using CodeFirst.Domain;
using CodeFirst.GraphQLSpecific;
using HotChocolate;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;

namespace CodeFirst.Resolvers
{

    public class CommonResolvers
    {
        public IEnumerable<PersonDbRecord> GetFriendsByPerson(
            int? id,string? name, Gender? gender,HairColor? hairColor,
            [Parent]PersonDbRecord person, //siin sees on personi info
            [Service]DataContext data)
        {
            var peopleQuery = data.GetPeople(id, name, gender, hairColor);

            peopleQuery = peopleQuery.Where(friend => person.FriendIdsList.Contains(friend.Id));

            return peopleQuery;
        }

        public IEnumerable<CarDbRecord> GetCarsByPerson(
            int? id, int? modelYear, KnownColor? color, string? modelName, string? manufacturer,
            [Parent]PersonDbRecord person, //siin sees on personi info
            [Service]DataContext data)
        {
            var carQuery = data.GetCars(id, modelYear, color, modelName,manufacturer)
                .Where(x => x.OwnerId == person.Id);

            return carQuery;
        }

        public PersonDbRecord GetOwnerByCar(
            [Parent]CarDbRecord car, //siin sees on cari info
            [Service]DataContext data)
        {
            var person = data.GetPerson(car.OwnerId);
            return person;
        }

    }
}
