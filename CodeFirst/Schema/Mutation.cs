﻿using HotChocolate;
using HotChocolate.Subscriptions;
using System.Threading.Tasks;
using CodeFirst.Domain;

namespace CodeFirst
{
    public class Mutation
    {
        private readonly DataContext _data;
        public Mutation(DataContext data)
        {
            _data = data;
        }


        public async Task<string> CreatePerson(
            PersonDbRecord person,
            [Service]IEventSender eventSender)
        {
            _data.AddPerson(person);
            
            return "added person successfully";
        }

        public async Task<string> AddFriend(
            int personId,int friendId,
            [Service]IEventSender eventSender)
        {
            _data.AddFriend(personId,friendId);
            
            return "added friend successfully";
        }

        public async Task<string> CreateCar(
            CarDbRecord car,
            [Service]IEventSender eventSender)
        {
            _data.AddCar(car);
            
            return "added car successfully";
        }
    }
}
