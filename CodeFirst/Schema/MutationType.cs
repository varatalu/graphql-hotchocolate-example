﻿using HotChocolate.Types;

namespace CodeFirst.Types
{
    public class MutationType
        : ObjectType<Mutation>
    {
        protected override void Configure(IObjectTypeDescriptor<Mutation> descriptor)
        {
            descriptor.Field(x => x.AddFriend(default, default,default))
                .Type<StringType>();

            descriptor.Field(x => x.CreateCar( default,default))
                .Type<StringType>();

            descriptor.Field(x => x.CreatePerson( default,default))
                .Type<StringType>();
        }
    }
}
