using CodeFirst.Domain;
using CodeFirst.GraphQLSpecific;
using System.Collections.Generic;
using System.Drawing;

namespace CodeFirst
{
    public class Query
    {
        private readonly DataContext _data;

        public Query(DataContext data)
        {
            _data = data;
        }

        //public class PersonQueryArgs
        //{
        //    public int? count { get; set; }
        //    public int? id { get; set; }
        //    public HairColor? hairColor { get; set; }
        //    public Gender? gender { get; set; }
        //}

        public IEnumerable<PersonDbRecord> GetPeople(int? id,string? name,HairColor? hairColor, Gender? gender)
        {
            return _data.GetPeople(id,name,gender,hairColor);
        }

        public IEnumerable<CarDbRecord> GetCars(int? id,int? modelYear ,KnownColor? color, string? modelName, string? manufacturer)
        {
            return _data.GetCars(id,modelYear,color,modelName,manufacturer);
        }
       
    }
}
