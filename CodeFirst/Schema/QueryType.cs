﻿using CodeFirst.GraphQLSpecific;
using HotChocolate.Types;

namespace CodeFirst.Types
{
   
    public class QueryType
        : ObjectType<Query>
    {
        protected override void Configure(IObjectTypeDescriptor<Query> descriptor)
        {
         
            descriptor.Field(t => t.GetPeople(default,default,default,default))
                .Type<ListType<PersonType>>();

            descriptor.Field(t => t.GetCars(default, default, default, default,default))
                .Type<ListType<CarType>>();
        }
    }
}
