﻿using System.Drawing;
using System.Linq;
using CodeFirst.Domain;
using CodeFirst.Resolvers;
using HotChocolate.Types;

namespace CodeFirst.GraphQLSpecific
{
    public class CarType : ObjectType<CarDbRecord>
    {
    
        protected override void Configure(IObjectTypeDescriptor<CarDbRecord> descriptor)
        {

            descriptor.Field(t => t.Id)
                .Type<NonNullType<IdType>>();

            descriptor.Field(t => t.Manufacturer)
                .Type<StringType>();

            descriptor.Field(t => t.ModelName)
                .Type<StringType>();

            descriptor.Field(t => t.ModelYear)
                .Type<IntType>();

            descriptor.Field(t => t.OwnerId)
                .Type<IntType>();

            descriptor.Field(t => t.Color)
                .Type<EnumType<KnownColor>>();


            descriptor.Field<CommonResolvers>(r => r.GetOwnerByCar(default,default))
                .Name("owner");
        }
    }
}
