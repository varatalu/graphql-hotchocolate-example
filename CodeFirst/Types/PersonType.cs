﻿using System.Linq;
using CodeFirst.Domain;
using CodeFirst.Resolvers;
using CodeFirst.Types;
using GreenDonut;
using HotChocolate.Resolvers;
using HotChocolate.Types;
using HotChocolate.Types.Relay;

namespace CodeFirst.GraphQLSpecific
{
    public class PersonType : ObjectType<PersonDbRecord>
    {
        protected override void Configure(IObjectTypeDescriptor<PersonDbRecord> descriptor)
        {

            descriptor.Field(t => t.Id)
                .Type<NonNullType<IdType>>();

            descriptor.Field(t => t.Name)
                .Type<StringType>();

            descriptor.Field(t => t.Height)
                .Type<StringType>();

            descriptor.Field(t => t.Weight)
                .Type<StringType>();

            descriptor.Field(t => t.DateOfBirth)
                .Type<DateTimeType>();

            descriptor.Field(t => t.Gender)
                .Type<EnumType<Gender>>();

            descriptor.Field(t => t.HairColor)
                .Type<EnumType<HairColor>>();

            descriptor.Field<CommonResolvers>(r => r.GetFriendsByPerson(default,default,default,default,default, default))
                .Name("friends");

            descriptor.Field<CommonResolvers>(r => r.GetCarsByPerson(default, default,default,default,default,default,default)) 
                .Name("cars");
        }
    }
}
